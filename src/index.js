import StripeCheckout from './StripeCheckout.vue';

const VueStripe = {
    StripeCheckout
};

module.exports = VueStripe;
